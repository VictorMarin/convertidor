package convertidor.victormarin.facci.convertidor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText cantidad;
    RadioButton cels, fare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cantidad = (EditText) findViewById(R.id.CantidadEditText);
        cels = (RadioButton) findViewById(R.id.paraCelcios);
        fare = (RadioButton) findViewById(R.id.paraFarenheit);
        Log.e("MainActivity", "Víctor Manuel Marín Arcentales");
    }
    public void convertir(View v){
        double value = new Double(cantidad.getText().toString());

        if(cels.isChecked()){
            value = Convertidor.celcios(value);
            Toast.makeText(this,"la conversion es :  " + value + "  °C",Toast.LENGTH_LONG).show();
        }
        else {
            value = Convertidor.Farenheit(value);
            Toast.makeText(this,"la conversion es :  " + value + "  °F",Toast.LENGTH_LONG).show();

        }
    }
}
